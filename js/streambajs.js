$( function() {

    $(".topMenuBox").hover( function(e) {

        $el = $(this);

        $el.hoverFlow(e.type, {
            height: "60px"
        }, 'fast');

    }, function(e) {

        $el = $(this);

        $el.hoverFlow(e.type, {
            height: "20px"
        }, 'fast');
    });
    
    $('#slideshow').cycle({
      timeout: 6000
    });
});
